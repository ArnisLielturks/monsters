FROM node:14

WORKDIR /app

COPY . /app

RUN npm install

ENTRYPOINT ["node", "/app/main.js"]

CMD ["--monster-count=1000", "--turn-limit=10000"]
