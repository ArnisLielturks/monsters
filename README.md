# Assumptions

* Game is stopped when the remaining monster count is < 2
* If the city has more than 2 monsters, all of them get destroyed together with the city
* Monsters have unique names
* All cities have unique names
* Input file format is not checked, we assume that it's always correct

# Building and running the application

This project can be built in multiple ways - by using docker or installing and running everything locally.

## 1. Docker

Run the following commands to build and run the container.
Game map will be stored in the same directory from which the container was started. 
e.g. if the container is ran from the `/home` directory, the output will be stored in the `/home/world_map_medium.txt`.

```shell
docker build -t monsters .
docker run -it -v$(pwd)/outputs/:/app/outputs monsters --monster-count=1 --turn-limit=1
```

## 2. Local

Game map will be stored in the `outputs` folder.
```bash
npm install
node main.js --monster-count=1 --turn-limit=1
```


## Running tests
To run application tests, run the following command
```bash
npm run test
```

