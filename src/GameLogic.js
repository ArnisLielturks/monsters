/**
 * All the game related logic goes here
 */
class GameLogic {
    /**
     * ctor
     * @param map - Map object
     * @param config - configuration
     */
    constructor(map, config) {
        this.map = map;
        this.turnLimit = 10000;
        if (typeof config === 'object') {
            if (Object.prototype.hasOwnProperty.call(config, 'monster_count')) {
                this.map.spawnMonsters(config.monster_count);
            }
            if (Object.prototype.hasOwnProperty.call(config, 'turn_limit')) {
                this.turnLimit = config.turn_limit;
            }
        }
    }

    /**
     * Move monsters between cities
     */
    makeMonstersMove() {
        Object.keys(this.map.monsters).forEach((monster) => {
            const sourceCity = this.map.getMonsterCity(monster);
            if (!sourceCity) {
                console.error('No city found for monster', monster, this.map.monsters[monster]);
                return;
            }
            const targetCityName = sourceCity.getRandomNeighborCity();
            const targetCity = this.map.getCityByName(targetCityName);
            if (!targetCity) {
                // No paths lead out of the city, monster is stuck!
                return;
            }

            this.map.moveMonsterToCity(monster, sourceCity, targetCity);
        });
        this.map.cities.forEach((city) => {
            city.endMove();
        });
    }

    /**
     * Check which cities contain more monsters than it should
     * and remove cities and their paths which have monsters in them
     */
    checkMonsterColissions() {
        const removeCityQueue = [];
        this.map.cities.forEach((city) => {
            // TODO: 2 or more monsters?
            if (city.monsters.length > 1) {
                console.log('City', city.name, 'has been destroyed by', city.monsters.join(', '));
                city.monsters.forEach((monster) => {
                    this.map.removeMonster(monster);
                });
                removeCityQueue.push(city.name);
            }
        });

        for (let i = removeCityQueue.length - 1; i >= 0; i--) {
            const cityName = removeCityQueue[i];
            this.map.removeCity(cityName);
        }
        this.map.recalculateCityIndexes();
    }

    /**
     * Run singe game loop
     * @returns {number} Number of monsters still in the game
     */
    run() {
        // console.log('Cities', this.cities);
        this.makeMonstersMove();
        // console.log('Active cities', this.map.cities.length);
        this.checkMonsterColissions();
        // console.log('Remaining monsters after this turn', Object.keys(this.map.monsters).length);
        return Object.keys(this.map.monsters).length;
    }

    /**
     * Start the game loop
     */
    start() {
        for (let i = 0; i < this.turnLimit; i++) {
            // console.log('--- TURN', (i + 1), '---');
            const nowRemaining = this.run();

            if (nowRemaining < 2) {
                this.endGame();
                return;
            }
        }
        this.endGame();
    }

    /**
     * Print out end game statistics
     */
    endGame() {
        console.log('---');
        console.log('Active cities', this.map.cities.length);
        console.log('Game ended, total turns calculated:', this.turnLimit);
    }
}

module.exports = GameLogic;
