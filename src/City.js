/**
 * City object that holds information about neighboring cities
 */
class City {
    /**
     * ctor
     * @param name - name of the city
     * @param generator - random number generator function
     */
    constructor(name, generator) {
        this.name = name;
        /**
         * Holds all the paths that go out of the city
         */
        this.paths = {};
        /**
         * Holds the list of active paths
         */
        this.directions = [];
        /**
         * Monsters currently in the city
         */
        this.monsters = [];
        /**
         * Monsters that will be placed in the city at the end of turn
         */
        this.pendingMonsters = [];
        this.generator = generator;
    }

    /**
     * Add path to the neighbor city
     * Input value should be in format "[direction]=[city_name]"
     * @param value
     */
    addPath(value) {
        const data = value.split('=');
        if (data.length !== 2) {
            console.error('Failed to add path to neighbor city, invalid input:', value);
            return;
        }
        const direction = data[0];
        const targetCity = data[1];
        this.paths[direction] = targetCity;
        // console.log('Adding path from', this.name, 'to', data[0], 'city', data[1], '');
        this.directions.push(direction);
    }

    /**
     * Remove path to the city
     * @param cityName - city name which paths is no longer valid
     */
    removePath(cityName) {
        Object.keys(this.paths).forEach((direction) => {
            if (cityName === this.paths[direction]) {
                const index = this.directions.indexOf(direction);
                if (index >= 0) {
                    // console.log('Removing path from ', this.name, 'to', cityName);
                    this.directions.splice(index, 1);
                }
            }
        });
    }

    /**
     * Get random neighboring city to which it has a valid path
     * @returns {null|*}
     */
    getRandomNeighborCity() {
        if (this.directions.length === 0) {
            return null;
        }
        const directionIndex = this.generator(this.directions.length);
        const directionName = this.directions[directionIndex];
        return this.paths[directionName];
    }

    /**
     * When monsters have been moved around, assign them to the city.
     * NOTE: This is a crucial step so that each monster is moved around only once per turn.
     */
    endMove() {
        this.monsters = Array.from(this.pendingMonsters);
        this.pendingMonsters = [];
    }

    /**
     * Mark the monster for placing in the city.
     * To place them in the city directly, us the force option
     * @param monsterName - monster name to add
     * @param forceAdd - add the monster to the city directly
     */
    addMonster(monsterName, forceAdd) {
        if (forceAdd) {
            this.monsters.push(monsterName);
        } else {
            this.pendingMonsters.push(monsterName);
        }
    }

    /**
     * Remove monster from the city
     * @param monsterName - name of the monster to remove
     */
    removeMonster(monsterName) {
        const index = this.monsters.indexOf(monsterName);
        if (index >= 0) {
            this.monsters.splice(index, 1);
        }
    }
}

module.exports = City;
