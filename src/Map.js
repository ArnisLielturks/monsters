const fs = require('fs');
const Random = require('random-seed');
const City = require('./City');

/**
 * Grid that contains information about all the cities
 */
class Map {
    /**
     * ctor
     * @param seed - random seed for number generator
     */
    constructor(seed) {
        this.cities = [];
        /**
         * Optimization that caches city position in the array
         * key - city name
         * value - index in the cities array
         */
        this.cityHashMap = {};
        /**
         * Active monsters in the map
         * key - monster name
         * value - city name where monster is located
         */
        this.monsters = {};

        /**
         * Generator allows us to use seed to generate random numbers.
         */
        this.generator = new Random.create(seed);
    }

    /**
     * Load the map from the string
     * @param content - map txt file content
     */
    load(content) {
        const lines = content.split('\n');
        lines.forEach((line) => {
            const data = line.split(' ');
            if (data.length <= 1) {
                return;
            }

            const city = new City(data[0], this.generator);

            // All the remaining parts of the input line define neighbor city paths
            for (let i = 1; i < data.length; i++) {
                city.addPath(data[i]);
            }
            this.addCity(city);
        });

        console.log('Map loaded, total', this.cities.length, 'cities loaded');
    }

    /**
     * Add the city to the map if it doesnt exist already
     * @param city - add new city object to the map
     */
    addCity(city) {
        // TODO: validate if the city doesn't exist already
        this.cities.push(city);
        this.cityHashMap[city.name] = this.cities.length - 1;
    }

    /**
     * Remove city from the map
     * @param cityName
     */
    removeCity(cityName) {
        if (!Object.prototype.hasOwnProperty.call(this.cityHashMap, cityName)) {
            console.error('Cannot remove city from the map', cityName);
            return;
        }
        const cityIndex = this.cityHashMap[cityName];
        this.cities.splice(cityIndex, 1);
        this.cities.forEach((city) => {
            city.removePath(cityName);
        });
    }

    /**
     * Spawn monsters in the cities
     * @param count
     */
    spawnMonsters(count) {
        if (this.cities.length === 0) {
            console.error('No cities added to the map');
            return;
        }
        for (let i = 0; i < count; i++) {
            const monsterName = `Monster${i + 1}`;
            const cityIndex = this.generator(this.cities.length);
            const city = this.cities[cityIndex];
            this.placeMonsterInCity(monsterName, city);
        }
        console.log('Spawned', count, 'monsters');
    }

    /**
     * Place monster in specific city
     * @param monsterName
     * @param city
     */
    placeMonsterInCity(monsterName, city) {
        city.addMonster(monsterName, true);
        this.monsters[monsterName] = city.name;
    }

    /**
     * Move monster between cities
     * @param monster - name of the monster
     * @param sourceCity - from which city to move the monsters
     * @param targetCity - to which city to move monster
     */
    moveMonsterToCity(monster, sourceCity, targetCity) {
        sourceCity.removeMonster(monster);
        targetCity.addMonster(monster);
        this.monsters[monster] = targetCity.name;
    }

    /**
     * Remove monster from the map
     * @param monster - name of the monster
     */
    removeMonster(monster) {
        delete (this.monsters[monster]);
    }

    /**
     * Optimize city lookup by their name
     * HashMap links city name to the location in the cities array
     */
    recalculateCityIndexes() {
        this.cities.forEach((city, index) => {
            this.cityHashMap[city.name] = index;
        });
    }

    /**
     * Get city object where the monster is currently located
     * @param monster
     * @returns {null|*}
     */
    getMonsterCity(monster) {
        const cityName = this.monsters[monster];
        const cityIndex = this.cityHashMap[cityName];
        if (cityIndex < 0) {
            return null;
        }
        return this.cities[cityIndex];
    }

    /**
     * Lookup city by it's name
     * @param cityName
     * @returns {null|*}
     */
    getCityByName(cityName) {
        if (Object.prototype.hasOwnProperty.call(this.cityHashMap, cityName)) {
            const index = this.cityHashMap[cityName];
            return this.cities[index];
        }

        return null;
    }

    generateDump() {
        let content = '';
        this.cities.forEach((city) => {
            content += `${city.name} `;
            city.directions.forEach((direction) => {
                content += `${direction}=${city.paths[direction]} `;
            });
            content += '\n';
        });
        return content;
    }

    /**
     * Dump the map to the specified file
     * @param location - where to save the file
     */
    dumpToFile(location) {
        fs.writeFileSync(location, this.generateDump());
        fs.chmod(location, 0o777, () => {});
        console.log('Saved map to', location);
    }
}

module.exports = Map;
