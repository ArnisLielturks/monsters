const assert = require('assert');
const City = require('../src/City');

describe('Test city object', () => {
    it('Should add neighbor city', () => {
        const city = new City('ThisCity', null);
        city.addPath('south=OtherCity');
        assert.equal(city.paths['south'], 'OtherCity');
        assert.equal(city.directions[0], 'south');
    });

    it('Should not add neighbor city', () => {
        const city = new City('ThisCity', null);
        city.addPath('south-OtherCity');
        assert.notEqual(city.paths['south'], 'OtherCity');
        assert.equal(city.directions.length, 0);
    });

    it('Should remove path', () => {
        const city = new City('ThisCity', null);
        city.addPath('south=OtherCity');
        assert.equal(city.directions.length, 1);
        city.removePath('OtherCity');
        assert.equal(city.paths['south'], 'OtherCity');
        assert.equal(city.directions.length, 0);
    });

    it('Should get random neighbor city', () => {
        const city = new City('ThisCity', function(num) {
            assert.equal(num, 4);
            return 3;
        });
        city.addPath('south=SouthCity');
        city.addPath('west=WestCity');
        city.addPath('east=EastCity');
        city.addPath('north=NorthCity');
        assert.equal(city.directions.length, 4);
        assert.equal(city.getRandomNeighborCity(), 'NorthCity');
    });

    it('Should not get random neighbor city', () => {
        const city = new City('ThisCity', function(num) {
            assert.equal(num, 4);
            return 3;
        });
        assert.equal(city.directions.length, 0);
        assert.equal(city.getRandomNeighborCity(), null);
    });

    it('Should finish monster moving', () => {
        const city = new City('ThisCity');
        city.addMonster('monster1')
        assert.equal(city.monsters.length, 0);
        assert.equal(city.pendingMonsters.length, 1);
        city.endMove();
        assert.deepEqual(city.monsters, ['monster1']);
        assert.equal(city.pendingMonsters.length, 0);
    });

    it('Should force add monster', () => {
        const city = new City('ThisCity');
        city.addMonster('monster1', true)
        assert.equal(city.monsters.length, 1);
        assert.equal(city.pendingMonsters.length, 0);
        assert.deepEqual(city.monsters, ['monster1']);
    });

    it('Should remove monster', () => {
        const city = new City('ThisCity');
        city.addMonster('monster1')
        city.endMove();
        assert.deepEqual(city.monsters, ['monster1']);
        city.removeMonster('monster1');
        assert.equal(city.monsters.length, 0);
    });
});

