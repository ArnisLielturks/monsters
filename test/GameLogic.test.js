const assert = require('assert');
const GameLogic = require('../src/GameLogic');
const Map = require('../src/Map');

describe('Test GameLogic object', () => {
    it('Should load config', () => {
        const map = new Map('random_seed');
        let content = 'FirstCity north=NorthCity south=SouthCity';
        map.load(content);
        const game = new GameLogic(map, {
            monster_count: 100,
            turn_limit: 15,
        });
        assert.equal(Object.keys(map.monsters).length, 100);
        assert.equal(game.turnLimit, 15);
    });

    it('Should not load config', () => {
        const map = new Map('random_seed');
        let content = 'FirstCity north=NorthCity south=SouthCity';
        map.load(content);
        const game = new GameLogic(map);
        assert.equal(Object.keys(map.monsters).length, 0);
        assert.equal(game.turnLimit, 10000);
    });

    it('Move monster between 2 connected cities', () => {
        const map = new Map('random_seed');
        let content = 'FirstCity north=SecondCity';
        content += '\nSecondCity south=FirstCity';
        map.load(content);
        const game = new GameLogic(map, {
            monster_count: 1,
        });

        const city1Monsters = map.cities[0].monsters.length;
        const city2Monsters = map.cities[1].monsters.length;

        assert.equal(city1Monsters, map.cities[0].monsters.length);
        assert.equal(city2Monsters, map.cities[1].monsters.length);

        game.makeMonstersMove();

        // Monster should be moved to the other city
        assert.equal(city2Monsters, map.cities[0].monsters.length);
        assert.equal(city1Monsters, map.cities[1].monsters.length);
    });

    it('Move monster around the map with multiple paths', () => {
        console.log(' ------');
        const map = new Map('random_seed');
        let content = 'FirstCity north=ThirdCity';
        content += '\nSecondCity west=ThirdCity';
        content += '\nThirdCity south=FirstCity east=SecondCity';
        map.load(content);
        // Spawn monster in the first city
        map.cities[0].addMonster('monster1', true);
        // Add the monster to the map
        map.monsters['monster1'] = map.cities[0].name;
        const game = new GameLogic(map);

        assert.equal(map.cities.length, 3);

        assert.equal(map.cities[0].monsters.length, 1);
        assert.equal(map.cities[1].monsters.length, 0);
        assert.equal(map.cities[2].monsters.length, 0);
        game.makeMonstersMove();

        assert.equal(map.cities[0].monsters.length, 0);
        assert.equal(map.cities[1].monsters.length, 0);
        assert.equal(map.cities[2].monsters.length, 1);
    });

    it('Check collisions', () => {
        const map = new Map('random_seed');
        let content = 'FirstCity north=SecondCity';
        content += '\nSecondCity south=FirstCity';
        map.load(content);
        map.cities[0].addMonster('monster1', true);
        map.cities[0].addMonster('monster2', true);
        const game = new GameLogic(map);

        assert.equal(map.cities.length, 2);
        assert.equal(map.cities[0].name, 'FirstCity');
        assert.equal(map.cities[1].name, 'SecondCity');
        game.checkMonsterColissions();
        assert.equal(map.cities.length, 1);
        assert.equal(map.cities[0].name, 'SecondCity');
    });

    it('Check game loop', () => {
        const map = new Map('random_seed');
        let content = 'FirstCity north=SecondCity';
        content += '\nSecondCity south=FirstCity';
        map.load(content);
        // Add 3 monsters just so that exactly 1 city gets destroyed
        const game = new GameLogic(map, {
            monster_count: 3,
        });

        assert.equal(map.cities.length, 2);
        assert.equal(Object.keys(map.monsters).length, 3);
        let remainingMonsters = game.run();
        assert.equal(map.cities.length, 1);
        assert.equal(Object.keys(map.monsters).length, remainingMonsters);
        assert.equal(Object.keys(map.monsters).length <= 1, true)
    });

    it('Check full game run', () => {
        const map = new Map('random_seed');

        // Just so that we can predict where the monsters will be moved each turn
        // In this case this is the last path in every city
        map.generator = function(range) {
            return range - 1;
        };

        let content = 'A east=B south=C';
        content += '\nB west=A south=D';
        content += '\nC north=A east=D';
        content += '\nD north=B west=C south=F east=E';
        content += '\nE west=D';
        content += '\nF north=D';

        map.load(content);
        // Place one monster in each map
        map.placeMonsterInCity('monster1', map.cities[0]);
        map.placeMonsterInCity('monster2', map.cities[1]);
        map.placeMonsterInCity('monster3', map.cities[2]);
        map.placeMonsterInCity('monster4', map.cities[3]);
        map.placeMonsterInCity('monster5', map.cities[4]);
        map.placeMonsterInCity('monster6', map.cities[5]);

        // Run the actual game loop
        const game = new GameLogic(map);
        game.start();

        // Check if the city D was wiped from the map
        const result = map.generateDump();
        let expectedResult = 'A east=B south=C ';
        expectedResult += `\nB west=A `;
        expectedResult += '\nC north=A ';
        expectedResult += '\nE ';
        expectedResult += '\nF ';
        expectedResult += '\n';
        assert.equal(result, expectedResult);


    });
});

