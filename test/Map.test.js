const assert = require('assert');
const Map = require('../src/Map');
const City = require('../src/City');
const Random = require('random-seed');

describe('Test map object', () => {
    it('Should load cities', () => {
        const map = new Map(null);
        assert.equal(map.cities.length, 0);
        let content = 'FirstCity north=NorthCity south=SouthCity';
        content += '\nSecondCity north=NorthCity south=SouthCity';
        map.load(content);
        assert.equal(map.cities.length, 2);
        assert.equal(map.cities[0].name, 'FirstCity')
        assert.equal(map.cities[1].name, 'SecondCity');
    });

    it('Test that map adds city directly', () => {
        const map = new Map(null);
        assert.equal(map.cities.length, 0);
        map.addCity(new City('TestCity'));
        assert.equal(map.cities.length, 1);
        assert.equal(map.cities[0].name, 'TestCity')
    });

    it('Test monster spawning in 1 city', () => {
        const map = new Map(function (range) {
            return range - 1;
        });
        assert.equal(map.cities.length, 0);
        map.addCity(new City('TestCity'));
        assert.equal(map.cities.length, 1);
        assert.equal(map.cities[0].name, 'TestCity')
        map.spawnMonsters(10)
        assert.equal(Object.keys(map.monsters).length, 10);
        assert.equal(map.cities[0].monsters.length, 10);
    });

    it('Test monster spawning with not cities', () => {
        const map = new Map(function (range) {
            return range - 1;
        });
        assert.equal(map.cities.length, 0);
        map.spawnMonsters(10)
        assert.equal(Object.keys(map.monsters).length, 0);
    });

    it('Test monster spawning in multiple cities', () => {
        const map = new Map(new Random.create('random_seed'));
        assert.equal(map.cities.length, 0);
        map.addCity(new City('TestCity1'));
        map.addCity(new City('TestCity2'));
        map.addCity(new City('TestCity3'));
        map.addCity(new City('TestCity4'));
        map.spawnMonsters(10)
        assert.equal(Object.keys(map.monsters).length, 10);
        let spawnedMonsters = {};
        map.cities.forEach(city => {
            city.monsters.forEach(monster => {
                spawnedMonsters[monster] = city.name;
            })
        });
        assert.equal(Object.keys(spawnedMonsters).length, 10);
    });

    it('Test place monster in specific city', () => {
        const map = new Map(new Random.create('random_seed'));
        assert.equal(map.cities.length, 0);
        map.addCity(new City('TestCity1'));
        map.addCity(new City('TestCity2'));
        map.placeMonsterInCity('monster1', map.cities[0]);
        assert.equal(Object.keys(map.monsters).length, 1);
        assert.deepEqual(map.cities[0].monsters, ['monster1']);
    });

    it('Move monsters between cities', () => {
        const map = new Map(null);
        const city1 = new City('TestCity1');
        const city2 = new City('TestCity2');
        city1.addMonster('monster1', true);
        assert.equal(city1.monsters.length, 1);
        assert.equal(city1.monsters[0], 'monster1');
        assert.equal(city2.monsters.length, 0);
        assert.equal(city2.pendingMonsters.length, 0);
        map.moveMonsterToCity('monster1', city1, city2);
        assert.equal(city1.monsters.length, 0);
        assert.equal(city2.pendingMonsters.length, 1);
        assert.equal(city2.pendingMonsters[0], 'monster1');
    });

    it('Test remove monster from map', () => {
        const map = new Map(new Random.create('random_seed'));
        map.addCity(new City('TestCity1'));
        map.spawnMonsters(1)
        assert.equal(Object.keys(map.monsters).length, 1);
        assert.equal(map.cities[0].monsters.length, 1);
        for (let monster in map.monsters) {
            map.removeMonster(monster);
        }
        assert.equal(Object.keys(map.monsters).length, 0);
    });

    it('Test city indexes', () => {
        const map = new Map(new Random.create('random_seed'));
        map.addCity(new City('TestCity1'));
        map.addCity(new City('TestCity2'));
        assert.equal(map.cityHashMap['TestCity1'], 0);
        assert.equal(map.cityHashMap['TestCity2'], 1);
        map.removeCity('TestCity1');
        map.recalculateCityIndexes();
        assert.equal(map.cityHashMap['TestCity2'], 0);
    });

    it('Get monster city', () => {
        const map = new Map(new Random.create('random_seed'));
        assert.equal(map.cities.length, 0);
        const city = new City('TestCity1');
        map.addCity(city);
        map.spawnMonsters(1);
        for (let monster in map.monsters) {
            assert.deepEqual(map.getMonsterCity(monster), city)
        }
    });

    it('Get city by name', () => {
        const map = new Map(new Random.create('random_seed'));
        assert.equal(map.cities.length, 0);
        const city = new City('TestCity1');
        map.addCity(city);
        assert.equal(map.getCityByName('TestCity1'), city);
        assert.equal(map.getCityByName('TestCity2'), null);
    });
});

