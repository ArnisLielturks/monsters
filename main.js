const fs = require('fs');
const Map = require('./src/Map');
const GameLogic = require('./src/GameLogic');

/**
 * @module getArgs.js
 * get command line arguments (commands, named arguments, flags)
 *
 * @see https://stackoverflow.com/a/54098693/1786393
 *
 * @return {Object}
 *
 */
function getArgs() {
    const commands = [];
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach((arg) => {
            // long arg
            if (arg.slice(0, 2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2, longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            } else if (arg[0] === '-') {
                const flags = arg.slice(1, arg.length).split('');
                flags.forEach((flag) => {
                    args[flag] = true;
                });
            } else {
                // commands
                commands.push(arg);
            }
        });
    return { args, commands };
}

const args = getArgs();
const map = new Map('random_seed');

const filename = args.args.map || 'inputs/world_map_medium.txt';
const content = fs.readFileSync(filename, 'utf8');

map.load(content);

const game = new GameLogic(map, {
    monster_count: parseInt(args.args['monster-count'] || 100, 10),
    turn_limit: parseInt(args.args['turn-limit'] || 10000, 10),
});

game.start();

const outputFilePath = filename.split('/');
map.dumpToFile(`outputs/${outputFilePath[outputFilePath.length - 1]}`);
